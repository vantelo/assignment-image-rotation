#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#define BM 0x4D42
#pragma pack(push, 1)
struct bmp_header {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};
#pragma pack(pop)
static uint32_t getPadding(const uint64_t w) {
    const size_t widthSize = w * sizeof(struct pixel);
    if (widthSize % 4 == 0) return 0;
    return 4 - (widthSize % 4);
}

enum read_status check_header(struct bmp_header  header, FILE* in){
    const uint32_t hSize = sizeof(struct bmp_header);
    if (fread(&header, hSize, 1, in) < 1) {
        if (feof(in)) return READ_INVALID_BITS;
        return READ_INVALID_HEADER;
    }
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;
    if (header.biBitCount != 24) return READ_ERROR;
    if (header.bfType != BM) return READ_ERROR;
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header const header = {0};
    const uint32_t pSize = sizeof(struct pixel);
    enum read_status rs = check_header(header, in);

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(img->width * img->height * pSize);
    const uint32_t padding = getPadding(img->width);
    for (size_t j = 0; j < img->height; j++) {
        if (fread(img->data + j * img->width, pSize, img->width, in) < pSize) {
			free(img.data);
			rs = READ_INVALID_BITS;}
			
        if (fseek(in, padding, SEEK_CUR) != 0) rs = READ_INVALID_BITS;
    }
    if (fseek(in, 0, SEEK_SET) != 0) rs = READ_INVALID_BITS;
    return rs;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header const header =
            {
                    .bfType = BM,
                    .biBitCount = 24,
                    .biHeight = img->height,
                    .biWidth = img->width,
                    .bOffBits = sizeof(struct bmp_header),
                    .bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * img->width + img->width % 4) * img->height,
                    .biSizeImage = img->width * img->height * sizeof(struct pixel),
                    .biSize = 40,
                    .biPlanes = 1
            };
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_ERROR;
    }
    const uint32_t hSize = sizeof(struct bmp_header);
    const uint32_t pSize = sizeof(struct pixel);
    if (fwrite(&header, hSize, 1, out) < 1) return WRITE_ERROR;
    const char paddingBytes[3] = {0};
    const uint32_t padding = getPadding(img->width);

    for (size_t j = 0; j < img->height; j++) {
        if (fwrite(img->data + j * img->width, pSize, img->width, out) != img->width) return WRITE_ERROR;
        if (fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) return WRITE_ERROR;
        if (fflush(out) != 0) return WRITE_ERROR;
    }
    if (fseek(out, 0, SEEK_SET) != 0) return WRITE_ERROR;
    return WRITE_OK;
}