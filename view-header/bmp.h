#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"


enum read_status {
    READ_OK = 0,
    READ_ERROR,
    READ_INVALID_HEADER,
    READ_INVALID_BITS
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* pic);
enum write_status to_bmp(FILE* out, struct image const* pic);

#endif

