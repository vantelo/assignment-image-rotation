#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#include "image.h"

struct image rotate(struct image image);
#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_H